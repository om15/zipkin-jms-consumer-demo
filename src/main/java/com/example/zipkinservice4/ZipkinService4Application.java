package com.example.zipkinservice4;

import java.util.Date;

//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.zipkinservice4.JmsTracingConfiguration;

@EnableAutoConfiguration
@RestController
@CrossOrigin // So that javascript can be hosted elsewhere
@EnableJms
@Import(JmsTracingConfiguration.class)
public class ZipkinService4Application implements JmsListenerConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(ZipkinService4Application.class, args);
	}
	
	@Autowired
	RestTemplate restTemplate;

	@JmsListener(destination = "backend")
	public void onMessage() {
		System.err.println(new Date().toString());
	}

	@Override
	public void configureJmsListeners(JmsListenerEndpointRegistrar registrar) {
		SimpleJmsListenerEndpoint endpoint = new SimpleJmsListenerEndpoint();
		endpoint.setId("simple");
		endpoint.setDestination("backend");
		endpoint.setMessageListener(m -> onMessage());
		registrar.registerEndpoint(endpoint);
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	/*@GetMapping(value = "/zipkin4")
	public String zipkinService1() {
		LOG.info("Inside zipkinService 4..");
		return "Hi...";
	}*/
}
